//
//  ViewController.swift
//  SimplyWeather
//
//  Created by Jin Hong on 10/08/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import UIKit

class ViewController: UpdateViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    // MARK: - Properties
    // MARK: Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    private lazy var searchLocationButton: UIBarButtonItem = {
        let image = UIImage(named: "LocationArrow")
        let button = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(updateCurrentLocation))
        return button
    }()
    private lazy var searchCityButton: UIBarButtonItem = {
        let button = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(showCitySearchBar))
        return button
    }()
    private lazy var searchingIndicator: UIBarButtonItem = {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        let button = UIBarButtonItem(customView: indicator)
        indicator.startAnimating()
        return button
    }()
    private var refreshControl: UIRefreshControl!
    
    // MARK: Data
    private var _cities = Set<City>()
    private var cities: [City] { get {
        return Array(_cities).sorted(by: <) }}
    private var selectedCity: City?
    
    // MARK: - Functions
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Initial bar buttons
        navigationItem.rightBarButtonItem = searchCityButton
        navigationItem.leftBarButtonItem = searchLocationButton
        
        // Notification listening for location manager notifications
        NotificationCenter.default
            .addObserver(self, selector: #selector(locationUpdated), name: NSNotification.Name(rawValue: LocationManager.Update), object: nil)
        NotificationCenter.default
            .addObserver(self, selector: #selector(locationError), name: NSNotification.Name(rawValue: LocationManager.Error), object: nil)
        
        // Set up the refresh control
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshCities), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        // Retrieve from cache and update the table
        CacheService.retrieveCitiesFromCache { [unowned self] cities in
            self._cities = Set(cities)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
        // Load the data
        tableView.reloadData()
    }
    
    deinit {
        // Remove the location manager notification observers
        NotificationCenter.default
            .removeObserver(self, name: NSNotification.Name(rawValue: LocationManager.Update), object: nil)
        NotificationCenter.default
            .removeObserver(self, name: NSNotification.Name(rawValue: LocationManager.Error), object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWeather",
            let destination = segue.destination as? WeatherViewController {
            destination.city = selectedCity
        }
    }
    
    // MARK: Location Notifications
    /**
     Location manager location updated notification observer.
     Make the network request for weather data using location info.
     */
    @objc private func locationUpdated() {
        guard let coordinates = LocationManager.location?.coordinate else {
            // Reset the left bar button if no location returned
            setBarButton(left: true, button: searchLocationButton)
            return
        }
        
        // Make the request and update UI
        NetworkService.requestWeather(lat: coordinates.latitude, long: coordinates.longitude) { city in
            // Reset the left bar button after weather update using location
            self.setBarButton(left: true, button: self.searchLocationButton)
            
            // Add the city to the UI if returned
            guard let city = city else { return }
            self.addCity(city)
        }
    }
    
    @objc private func locationError() {
        // Reset the left bar button
        setBarButton(left: true, button: searchLocationButton)
        
        // Alert location error
        let title = "Location Error"
        let message = "Could not determine your current location. Please try to search your city."
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: Action
    @objc private func updateCurrentLocation() {
        // Update the button
        setBarButton(left: true, button: searchingIndicator)
        
        // Request the location
        LocationManager.requestLocation()
    }
    
    @objc private func showCitySearchBar() {
        toggleSearchBar(show: true)
    }
    
    /**
     Set the left or right bar button item
     - parameter left: left bar button item else right
     - parameter button: button to set to
     */
    private func setBarButton(left: Bool, button: UIBarButtonItem) {
        DispatchQueue.main.async {
            if left {
                self.navigationItem.leftBarButtonItem = button
            }
            else {
                self.navigationItem.rightBarButtonItem = button
            }
        }
    }
    
    // MARK: Table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let city = cities[(indexPath as NSIndexPath).row]
        let weather = city.weathers.first
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath)
        
        // Detail label text
        let updatedDate = weather?.date.isToday() == true ?
            "Today" :
            weather?.date.isYesterday() == true ?
                "Yesterday" :
            weather?.dateString
        
        // Set the cell text labels
        cell.textLabel?.text = city.name
        cell.detailTextLabel?.text = updatedDate
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedCity = cities[(indexPath as NSIndexPath).row]
        performSegue(withIdentifier: "showWeather", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        let city = cities[(indexPath as NSIndexPath).row]
        deleteCity(city)
    }
    
    // MARK: Data update
    /**
     Add the city to the cities set and reload the table view.
     Replaces an existing city if the ids match.
     */
    private func addCity(_ city: City) {
        if _cities.contains(city) {
            // City exists in set, so replace
            _cities.remove(city)
            _cities.insert(city)
            
            // Get index of refreshed city for cell update
            let i = cities.index { $0.id == city.id }!.advanced(by: 0)
            let indexPath = IndexPath(row: i, section: 0)
            
            // Update the table view UI in main thread
            DispatchQueue.main.async {
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        }
        else {
            // City not existing in set, so add it to the set and table view
            _cities.insert(city)
            let i = cities.index { $0.id == city.id }!.advanced(by: 0)
            let indexPath = IndexPath(row: i, section: 0)
            
            // Update the table view UI in main thread
            DispatchQueue.main.async {
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: [indexPath], with: .automatic)
                self.tableView.endUpdates()
            }
        }
    }
    
    /**
     Delete action for city.
     */
    private func deleteCity(_ city: City) {
        // Compute the index to remove the city from
        let i = cities.index { $0.id == city.id }!.advanced(by: 0)
        let indexPath = IndexPath(row: i, section: 0)
        
        // Remove the city from the view and file
        _cities.remove(city)
        CacheService.deleteCityFromFile(city)
        
        // Update the view
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
        }
    }
    
    /**
     Function for refresh control. Refetch data for all the cities.
     */
    @objc private func refreshCities() {
        let background = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
        background.async { [unowned self] in
            for city in self.cities {
                NetworkService.requestWeather(forCity: city.name) { city in
                    if let city = city {
                        self.addCity(city)
                    }
                }
            }
            // End refreshing after all cities have been refreshed
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    /**
     Retrieve the updated cities and reload the table view.
     */
    override func weatherUpdated() {
        CacheService.retrieveCitiesFromCache() { [weak self] cities in
            // Set the cities to the updated cities
            self?._cities = Set(cities)
            
            // Update the UI with refreshed data
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
    
    // MARK: Search bar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Hide the search bar
        toggleSearchBar(show: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Hide the search bar
        toggleSearchBar(show: false)
        
        // Make the request for the weather data
        guard let cityName = searchBar.text else { return }
        
        // Set the right bar button to indicator
        setBarButton(left: false, button: searchingIndicator)
        
        NetworkService.requestWeather(forCity: cityName) { city in
            // Reset the right bar button
            self.setBarButton(left: false, button: self.searchCityButton)
            
            // Add the city to the UI if returned
            guard let city = city else { return }
            self.addCity(city)
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        // Hide if user ends editing
        toggleSearchBar(show: false)
    }
    
    /**
     Toggle the search bar visibility.
     - parameter show: show or hide the search bar
     */
    private func toggleSearchBar(show: Bool) {
        // Become or resign the first responder for keyboard behaviour
        show ? searchBar.becomeFirstResponder() :
            searchBar.resignFirstResponder()
        
        // Set the search bar height and animate
        let height: CGFloat = show ? 44 : 0
        searchBarHeight.constant = height
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
}
