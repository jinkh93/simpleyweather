//
//  CacheService.swift
//  SimplyWeather
//
//  Created by Jin Hong on 13/08/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import UIKit

private extension FileManager {
    /**
     Check if file/directory already exists at given `path` otherwise create it.
     - parameter path: path to check file/directory existing else create.
     - returns: true if exists or successfully created.
     */
    static func existElseCreate(_ path: NSString) -> Bool {
        let path = path as String
        if `default`.fileExists(atPath: path) {
            // Do nothing if file/directory already exists
            return true
        }
        else {
            do {
                // Create the path if not existing
                try `default`.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
                return true
            } catch let error as Error {
                // Log error here
                print(error.localizedDescription)
                return false
            }
        }
    }
}

class CacheService {
    
    private static var cachePath: NSString {
        let cacheDir = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as NSString
        let appCacheDir = cacheDir.appendingPathComponent("SimplyWeather")
        return appCacheDir as NSString
    }
    
    /**
     Save the city response data to the cache file for restoration.
     The file writing happens in a background thread.
     - parameter data: the json data returned from the network call.
     */
    static func saveCityResponse(data: Data, forCity city: City) {
        // Use the city id as file name
        let filename = "\(city.id).json"
        
        // Write the json to file in background thread
        let background = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
        background.async { [filename] in
            // Confirm that the intermediary directory exists
            if FileManager.existElseCreate(cachePath) {
                let name = cachePath.appendingPathComponent(filename)
                FileManager.default.createFile(atPath: name, contents: data, attributes: nil)
            }
        }
    }
    
    /**
     Delete the city json file from the file system.
     */
    static func deleteCityFromFile(_ city: City) {
        let filename = "\(city.id).json"
        let manager = FileManager.default
        let path = cachePath.appendingPathComponent(filename)
        if manager.fileExists(atPath: path) {
            do {
                try manager.removeItem(atPath: path)
            } catch let error as Error {
                // Log error here
                print(error.localizedDescription)
            }
        }
    }
    
    /**
     Parse through the cache directory where the json data is saved and create the `City`
     objects and pass through on completion.
     */
    static func retrieveCitiesFromCache(onCompletion completion: @escaping (_ cities: [City]) -> Void) {
        let background = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
        background.async {
            
            let path = cachePath as String
            let manager = FileManager.default
            do {
                // Read through all .json files at cache directory
                let files = try manager.contentsOfDirectory(atPath: path)
                
                // Convert to `City` objects and complete
                let cities = files.flatMap { readCity(manager, filename: $0) }
                completion(cities)
            } catch let error as Error {
                // Log error here
                print(error.localizedDescription)
                completion([])
            }
        }
    }
    
    /**
     Perform a retrieve for the `City` object for the given `id`.
     */
    static func retrieveCity(forId id: Int) -> City? {
        let manager = FileManager.default
        let filename = "\(id).json"
        return readCity(manager, filename: filename)
    }
    
    /**
     Parse json file at `path` and create the `City` object.
     */
    private static func readCity(_ manager: FileManager, filename: String) -> City? {
        do {
            let path = cachePath.appendingPathComponent(filename)
            if let data = manager.contents(atPath: path) {
                let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let json = object as? Dict, let city = City(json: json) {
                    return city
                }
            }
            
            // Delete the file if not parsed as City object
            try manager.removeItem(atPath: path)
        } catch let error as Error {
            // Log error here
            print(error.localizedDescription)
        }
        
        return nil
    }
}
