# README #

This project builds and runs a simple weather application in order to show-case how to make network calls, access GPS location, reading/writing to file in iOS using swift 3.

This repository is available for anyone to use/copy/change in what-ever way they would like as a learning reference.

### Useful files to look at ###
* NetworkService.swift - For simple network calling requests using built in UrlSession.
* LocationManager.swift - For setting up the GPS location manager.
* CacheService.swift - For reading/writing to the device cache directory.

### Who do I talk to? ###

* jinkh93@gmail.com