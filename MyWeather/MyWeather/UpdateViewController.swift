//
//  UpdateViewController.swift
//  SimplyWeather
//
//  Created by Jin Hong on 15/09/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import UIKit

class UpdateViewController: UIViewController {
    static let WeatherUpdate = "WeatherUpdate"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add the weather update observer
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(weatherUpdated),
            name: NSNotification.Name(rawValue: UpdateViewController.WeatherUpdate),
            object: nil)
        
        // Refresh the weather for cities in the cache/file system
        updateData()
    }
    
    /**
     Calls the weather api for all cities in the cache/file system and
     posts the weather update notification once complete.
     */
    private func updateData() {
        CacheService.retrieveCitiesFromCache { cities in
            // Get the outdated cities in the cache/file system
            let outdated = cities.filter { city in
                return city.weathers.first?.date.isToday() != true
            }
            
            // Refresh all the outdated cities in the cache/file system
            let group = DispatchGroup()
            for city in outdated {
                group.enter()
                NetworkService.requestWeather(forCity: city.name) { _ in
                    group.leave()
                }
            }
            
            // Post the notification once all outdated cities are refreshed
            group.notify(queue: DispatchQueue.main) {
                NotificationCenter.default
                    .post(name: Notification.Name(rawValue: UpdateViewController.WeatherUpdate), object: nil)
            }
        }
    }
    
    deinit {
        // Remove the observer
        NotificationCenter.default.removeObserver(self)
    }
    
    /**
     Function to handle when the weather for all cities on device have updated.
     
     Default implementation is empty.
     */
    func weatherUpdated() {
        
    }
}
