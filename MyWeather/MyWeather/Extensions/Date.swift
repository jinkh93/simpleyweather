//
//  Date.swift
//  SimplyWeather
//
//  Created by Jin Hong on 14/08/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import Foundation

extension Date {
    /**
     Determine if the date timestamp is day or night.
     */
    func isNight() -> Bool {
        let calendar = Date.calendar()
        let hour = (calendar as NSCalendar).component(.hour, from: self)
        return hour > 12
    }
    
    /**
     Determine if the date is today or not.
     */
    func isToday() -> Bool {
        let calendar = Date.calendar()
        let isToday = calendar.isDateInToday(self)
        return isToday
    }
    
    /**
     Determine if the date is yesterday or not.
     */
    func isYesterday() -> Bool {
        let calendar = Date.calendar()
        let isYesterday = calendar.isDateInYesterday(self)
        return isYesterday
    }
    
    static func determineTimeOfDay(_ date: Date = Date()) -> Weather.TimeOfDay {
        let calendar = Date.calendar()
        let hour = (calendar as NSCalendar).component(.hour, from: date)
        switch (hour + 1) {
        case 1...3, 22...24: return .night
        case 4...9: return .morning
        case 10...15: return .day
        case 16...21: return .evening
        default: return .morning
        }
    }
    
    private static func gmtZeroTimezone() -> TimeZone {
        return TimeZone(secondsFromGMT: 0)!
    }
    
    static func calendar() -> Calendar {
        var calendar = Calendar.current
        calendar.timeZone = gmtZeroTimezone()
        return calendar
    }
    
    static func formatter(_ dateFormat: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        formatter.timeZone = gmtZeroTimezone()
        return formatter
    }
    
    static func formatter(_ dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateStyle = dateStyle
        formatter.timeStyle = timeStyle
        formatter.timeZone = gmtZeroTimezone()
        return formatter
    }
}
