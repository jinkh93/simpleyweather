//
//  Weather.swift
//  SimplyWeather
//
//  Created by Jin Hong on 12/08/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import Foundation

enum Icon: String {
    case
    Clear = "Clear",
    Cloud = "Clouds",
    Rain = "Rain",
    Snow = "Snow"
    
    /**
     Determine the letter representing the weather icon from Meteocons font.
     */
    func iconLetter(isNight night: Bool = false) -> String {
        if self == .Clear && night {
            return "C"
        }
        switch self {
        case .Clear: return "B"
        case .Cloud: return "N"
        case .Rain: return "R"
        case .Snow: return "G"
        }
    }
}

struct Weather {
    // Temperatures
    let morning: Float
    let day: Float
    let evening: Float
    let night: Float
    let max: Float
    let min: Float
    
    // Details
    let humidity: Float
    let pressure: Float
    let date: Date
    let icon: Icon?
    
    enum TimeOfDay {
        case morning, day, evening, night
    }
    
    init?(json: Dict) {
        // Unwrap the json
        // Temperatures
        guard let temps = json["temp"] as? Dict
            else { return nil }
        guard
            let morning = temps["morn"] as? Float,
            let day = temps["day"] as? Float,
            let evening = temps["eve"] as? Float,
            let night = temps["night"] as? Float,
            let max = temps["max"] as? Float,
            let min = temps["min"] as? Float
            else { return nil }
        
        // Details
        guard let dt = json["dt"] as? TimeInterval,
            let humidity = json["humidity"] as? Float,
            let pressure = json["pressure"] as? Float
            else { return nil }
        guard let iconJsons = json["weather"] as? [Dict],
            let iconJson = iconJsons.first,
            let icon = iconJson["main"] as? String
            else { return nil }
        
        // Set the fields
        self.morning = morning
        self.day = day
        self.evening = evening
        self.night = night
        self.max = max
        self.min = min
        self.humidity = humidity
        self.pressure = pressure
        self.date = Date(timeIntervalSince1970: dt)
        self.icon = Icon(rawValue: icon)
    }
    
    var dateString: String {
        let formatter = Date.formatter(.medium, timeStyle: .none)
        return formatter.string(from: date)
    }
    
    var dayString: String {
        let formatter = Date.formatter("EEE")
        return formatter.string(from: date)
    }
    
    var maxString: String {
        return String(format: "%.0f°", max)
    }
    
    var minString: String {
        return String(format: "%.0f°", min)
    }
}
