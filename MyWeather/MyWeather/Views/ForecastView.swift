//
//  ForecastView.swift
//  SimplyWeather
//
//  Created by Jin Hong on 13/08/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import UIKit

class ForecastView: UIView {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var maxLabel: UILabel!
    @IBOutlet weak var minLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialiseView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialiseView()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        initialiseView()
    }
    
    convenience init(weather: Weather) {
        self.init()
        setup(weather)
    }
    
    func setup(_ weather: Weather) {
        let night = Date().isNight()
        dayLabel.text = weather.dayString
        iconLabel.text = weather.icon?.iconLetter(isNight: night)
        maxLabel.text = weather.maxString
        minLabel.text = weather.minString
    }
    
    private func initialiseView() {
        // Instantiate from nib
        let view = Bundle.main.loadNibNamed("ForecastView", owner: self, options: nil)?[0] as! UIView
        addSubview(view)
        view.frame = bounds
    }
}
