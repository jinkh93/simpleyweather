//
//  NetworkService.swift
//  SimplyWeather
//
//  Created by Jin Hong on 10/08/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import Foundation
private func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

private func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l >= r
    default:
        return !(lhs < rhs)
    }
}


/**
 Extension for status code checking functionality
 */
private extension Optional where Wrapped: URLResponse {
    var statusCodeOk: Bool {
        let statusCode = (self as? HTTPURLResponse)?.statusCode
        return statusCode >= 200 && statusCode < 400
    }
}

/**
 GET request parameter builder by city name (user input) or location (user location)
 */
private class ParamBuilder {
    private static let appid = "002e60014d5cd6743672383fa28ce526"
    private static let units = "metric"
    private static let count = "5"
    
    static func buildCityParams(_ city: String) -> [String: AnyObject] {
        return ["q": city as AnyObject, "appid": appid as AnyObject, "units": units as AnyObject, "cnt": count as AnyObject]
    }
    
    static func buildLocationParams(_ lat: Double, _ long: Double) -> [String: AnyObject] {
        return ["lat": lat as AnyObject, "lon": long as AnyObject, "appid": appid as AnyObject, "units": units as AnyObject, "cnt": count as AnyObject]
    }
}

/**
 Network service with helper functions to make the weather requests.
 */
class NetworkService {
    
    private static let daily = "forecast/daily" // Forecast endpoint
    private static let url = URL(string: "http://api.openweathermap.org/data/2.5")! // Base url
    private static let session = URLSession.shared // Session
    
    /**
     Request the weather data for city.
     - parameters:
     - city: the city name
     - completion: completion block to asynchronously return the `Weather` object
     */
    static func requestWeather(forCity city: String, completion: @escaping (_ city: City?) -> Void) {
        // Build the request and perform it
        let params = ParamBuilder.buildCityParams(city)
        let request = buildGetRequest(daily, params: params)
        
        session.dataTask(with: request, completionHandler: { data, response, error in
            let city = processResponse(data, response, error)
            completion(city)
        }) .resume()
    }
    
    /**
     Request the weather data using lat, long data.
     - parameters:
     - lat: latitude
     - long: longitude
     - completion: completion block to asynchronously return the `Weather` object
     */
    static func requestWeather(lat: Double, long: Double, completion: @escaping (_ city: City?) -> Void) {
        // Build the request and perform it
        let params = ParamBuilder.buildLocationParams(lat, long)
        let request = buildGetRequest(daily, params: params)
        
        session.dataTask(with: request, completionHandler: { data, response, error in
            let city = processResponse(data, response, error)
            completion(city)
        }) .resume()
    }
    
    /**
     Process the `NSURLSessionDataTask` response. Since requesting the weather data
     using location params or city name params return the same response, this function
     can be reused for both.
     */
    private static func processResponse(_ data: Data?, _ response: URLResponse?, _ error: Error?) -> City? {
        // Ensure the data response is valid
        guard error == nil else { return nil }
        guard response.statusCodeOk else { return nil }
        guard let data = data else { return nil }
        
        // Process the json raw data
        var json: [String: AnyObject] = [:]
        do {
            if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject] {
                json = jsonResponse
            }
        } catch let error as Error {
            // Logging would go here
            print(error.localizedDescription)
        }
        
        // Parse the json data into a City object
        let city = City(json: json)
        
        if let city = city {
            // Save the response after successful parse
            CacheService.saveCityResponse(data: data, forCity: city)
        }
        
        return city
    }
    
    /**
     Private GET request builder
     */
    private static func buildGetRequest(_ endpoint: String, params: [String: AnyObject]) -> URLRequest {
        // Add the endpoint component
        var urlString = NetworkService.url.appendingPathComponent(endpoint).absoluteString
        urlString += params.isEmpty ? "" : "?"
        
        // Add the params into url for get request
        for (key, value) in params {
            urlString += "\(key)=\(value)&"
        }
        
        // Escape spaces
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? urlString
        
        // Create the GET request
        let url = URL(string: urlString)!
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        return request as URLRequest
    }
}
