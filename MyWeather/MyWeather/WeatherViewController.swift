//
//  WeatherViewController.swift
//  SimplyWeather
//
//  Created by Jin Hong on 12/08/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import UIKit

// MARK: - Weather view controller
class WeatherViewController: UpdateViewController {
    
    // MARK: Outlets
    // Main view
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    
    // Detail view
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    
    // MARK: Segue settable
    var city: City?
    var forecastView: ForecastViewController?
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = city?.name
        let displayed = displayCity()
        
        if !displayed {
            // Alert could not display correctly and pop back
            let title = "No Weather"
            let message = "Could not retrieve weather data. Please try again."
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default) { [unowned self] _ in
                self.navigationController?.popViewController(animated: true)
            }
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
        else {
            // Save the last viewed city
            UserDefaults.standard.set(city!.id, forKey: "LastViewedCity")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embedForecastView",
            let forecastViewController = segue.destination as? ForecastViewController {
            // Set the city weathers
            forecastViewController.weathers = city?.weathers ?? []
            
            // Keep reference to the forecast view to refresh when needed
            forecastView = forecastViewController
        }
    }
    
    // MARK: View update
    private func displayCity() -> Bool {
        guard let city = city else { return false }
        
        // Main view
        dateLabel.text = city.dateString
        tempLabel.text = city.tempString
        minTempLabel.text = city.minTempString
        maxTempLabel.text = city.maxTempString
        
        // Detail view
        pressureLabel.text = city.pressureString
        humidityLabel.text = city.humidityString
        
        // Background view
        view.backgroundColor = city.colorForTime
        
        return true
    }
    
    override func weatherUpdated() {
        // Set the refreshed city
        guard let city = city else { return }
        self.city = CacheService.retrieveCity(forId: city.id)
        
        // Reload the UI
        displayCity()
        forecastView?.weathers = city.weathers
        forecastView?.refreshForecast()
    }
}

// MARK: - Forecast view controller
class ForecastViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var stackView: UIStackView!
    
    // MARK: Data
    var weathers: [Weather] = []
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for weather in weathers {
            let weatherView = ForecastView(weather: weather)
            stackView.addArrangedSubview(weatherView)
        }
    }
    
    /**
     Refreshes the stack view forecast views with the weather data.
     */
    func refreshForecast() {
        let views = stackView.arrangedSubviews
        
        for (weather, view) in zip(weathers, views) {
            guard let weatherView = view as? ForecastView
                else { continue }
            weatherView.setup(weather)
        }
    }
}
