//
//  LocationManager.swift
//  SimplyWeather
//
//  Created by Jin Hong on 10/08/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    // Notification key
    static let Update = "Update"
    static let Error = "Auth"
    
    // Singleton instance and variables
    private static let instance: LocationManager = LocationManager(manager: CLLocationManager())
    private var manager: CLLocationManager
    private var location: CLLocation? // Latest location reference
    
    init(manager: CLLocationManager) {
        self.manager = manager
        super.init()
        self.manager.delegate = self
    }
    
    /**
     Latest location object received by the location manager.
     */
    static var location: CLLocation? {
        return instance.location
    }
    
    /**
     Perform the notification if a recent (newer than 12 hours) location exists.
     Otherwise request location on the location manager.
     */
    static func requestLocation() {
        if let location = instance.location {
            let elapsedTime = Date().timeIntervalSince(location.timestamp)
            if elapsedTime < (12 * 60 * 60) {
                NotificationCenter.default.post(name: Notification.Name(rawValue: LocationManager.Update), object: nil)
                return
            }
        }
        requestAuthorization(onAuthorized: instance.manager.requestLocation)
    }
    
    /**
     Location request authorization check.
     - parameter authorizedAction: the action to perform if authorized
     */
    private static func requestAuthorization(onAuthorized authorizedAction: (() -> Void)) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways, .authorizedWhenInUse: authorizedAction()
            case .notDetermined: instance.manager.requestWhenInUseAuthorization()
            default: // Denied, Restricted
                NotificationCenter.default.post(name: Notification.Name(rawValue: LocationManager.Error), object: nil)
            }
        }
        else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: LocationManager.Error), object: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // Save the latest location and post the notification
        location = locations.last
        NotificationCenter.default.post(name: Notification.Name(rawValue: LocationManager.Update), object: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager Error")
        print(error.localizedDescription)
        NotificationCenter.default.post(name: Notification.Name(rawValue: LocationManager.Error), object: nil)
    }
}
