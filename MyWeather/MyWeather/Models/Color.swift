//
//  Color.swift
//  SimplyWeather
//
//  Created by Jin Hong on 12/08/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import UIKit

private let ColorSpace: CGFloat = 255

struct Color {
    private static let rDay: CGFloat = 135
    private static let gDay: CGFloat = 170
    private static let bDay: CGFloat = 250
    private static let rNight: CGFloat = 70
    private static let gNight: CGFloat = 90
    private static let bNight: CGFloat = 130
    
    private static let dimGray: CGFloat = 220
    
    static var dayColor: UIColor {
        return UIColor(red: rDay/ColorSpace, green: gDay/ColorSpace, blue: bDay/ColorSpace, alpha: 1)
    }
    static var nightColor: UIColor {
        return UIColor(red: rNight/ColorSpace, green: gNight/ColorSpace, blue: bNight/ColorSpace, alpha: 1)
    }
    static var dimGreyColor: UIColor {
        let gray = dimGray/ColorSpace
        return UIColor(red: gray, green: gray, blue: gray, alpha: 1)
    }
    
    /**
     Apply a gradient for the base color (navy blue) to represent a brighter color
     during the day and a darker color during the night time.
     - parameter hour: a 0-indexed `Int` representing the hour of day in 24-hour space
     - note:
     day = 6am - 6pm
     
     night = 6pm - 6am
     */
    static func gradientColor(forHour hour: Int) -> UIColor {
        if hour == 0 { return nightColor }
        else if (hour + 1) == 12 { return dayColor }
        
        
        // Increment since argument is 0 indexed
        let hour = (hour + 1)%12
        
        // Gradient quotient and color space offset
        let q: CGFloat = 12/(CGFloat(hour))
        let rOffset = (rDay - rNight)/q
        let gOffset = (gDay - gNight)/q
        let bOffset = (bDay - bNight)/q
        
        // Calculate the r, g, b gradient value on hour of day
        let r, g, b: CGFloat
        if hour > 12 {
            // Day to night gradient
            r = rDay - rOffset
            g = gDay - gOffset
            b = bDay - bOffset
        }
        else {
            // Night to day gradient
            r = rNight + rOffset
            g = gNight + gOffset
            b = bNight + bOffset
        }
        return UIColor(red: r/ColorSpace, green: g/ColorSpace, blue: b/ColorSpace, alpha: 1)
    }
}
