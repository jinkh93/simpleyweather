//
//  AppDelegate.swift
//  SimplyWeather
//
//  Created by Jin Hong on 10/08/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow? { didSet {
        window?.tintColor = Color.nightColor }}
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Set up Fabric
        Fabric.sharedSDK().debug = true
        Fabric.with([Crashlytics.self])
        
        #if arch(i386) || arch(x86_64)
            print("CACHES DIRECTORY: \(FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask))")
        #endif
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let id = UserDefaults.standard.integer(forKey: "LastViewedCity")
        let city = CacheService.retrieveCity(forId: id)
        
        // Initialise the navigation stack with the city view controller on top
        if let navigation = window?.rootViewController as? UINavigationController {
            if navigation.childViewControllers.count == 1,
                let view = navigation.childViewControllers.first,
                let weather = storyboard.instantiateViewController(withIdentifier: "WeatherViewController") as? WeatherViewController,
                let city = city {
                // Set the city variable on the view controller
                weather.city = city
                
                // Set the navigation stack
                navigation.setViewControllers([view, weather], animated: false)
            }
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        // Save the user defaults for view restoration purposes.
        UserDefaults.standard.synchronize()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

