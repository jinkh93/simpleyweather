//
//  City.swift
//  SimplyWeather
//
//  Created by Jin Hong on 11/08/16.
//  Copyright © 2016 FoxEar. All rights reserved.
//

import UIKit

typealias Dict = [String: AnyObject]
struct City: Hashable, Comparable {
    let id: Int
    let name: String
    let weathers: [Weather]
    
    var hashValue: Int { return id }
    
    init?(json: Dict) {
        // Process the city data
        guard let city = json["city"] as? Dict,
            let id = city["id"] as? Int,
            let name = city["name"] as? String
            else { return nil }
        
        // Process the weather data list
        guard let list = json["list"] as? [Dict] else { return nil }
        let weathers = list.flatMap(Weather.init)
        guard weathers.count > 0 else { return nil }
        
        // Set the fields
        self.id = id
        self.name = name
        self.weathers = weathers
    }
    
    var dateString: String {
        let date = weathers.first!.date
        let formatter = Date.formatter("EEEE dd/MM")
        return formatter.string(from: date)
    }
    
    var tempString: String {
        let weather = weathers.first!
        let tod = Date.determineTimeOfDay(weather.date)
        let temp: Float
        switch tod {
        case .morning: temp = weather.morning
        case .day: temp = weather.day
        case .evening: temp = weather.evening
        case .night: temp = weather.night
        }
        return String(format: "%.0f°", temp)
    }
    
    var minTempString: String {
        let weather = weathers.first!
        return String(format: "%.0f°", weather.min)
    }
    
    var maxTempString: String {
        let weather = weathers.first!
        return String(format: "%.0f°", weather.max)
    }
    
    var pressureString: String {
        let weather = weathers.first!
        return String(format: "%.1f", weather.pressure)
    }
    
    var humidityString: String {
        let weather = weathers.first!
        return String(format: "%.1f", weather.humidity)
    }
    
    var colorForTime: UIColor {
        let weather = weathers.first!
        let calendar = Calendar.current
        let hour = (calendar as NSCalendar).component(.hour, from: weather.date as Date)
        let color = Color.gradientColor(forHour: hour)
        return color
    }
}
func ==(lhs: City, rhs: City) -> Bool {
    return lhs.id == rhs.id
}
func <(lhs: City, rhs: City) -> Bool {
    return lhs.name < rhs.name
}
